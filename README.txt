CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

Search API Attachments in Body Field allows Search API to index the contents of
files referenced in body fields.

In order for these files to be detected and indexed, the file needs to be:

 * managed by Drupal (i.e.: in the `file_managed` table, e.g.: uploaded with the
   Media module), and,
 * referenced with a full, absolute URL to the file (it can be in an HTML
   attribute), and,
 * the hostname in the URL needs to match the hostname that Drupal detects that
   it is being accessed from.

That is to say, if your site is at 'http://example.com/', then the following
HTML snippets will cause the file to be indexed...

    http://example.com/sites/default/test.txt
    <a href="http://example.com/sites/default/test.txt">My file</a>

... but the following HTML snippets will (currently) not cause the file to be
indexed...

    /sites/default/test.txt
    <a href="/sites/default/test.txt">My file</a>
    public://test.txt
    <a href="public://test.txt">My file</a>
    [[{"fid":"1",...}]]

REQUIREMENTS
------------

 * Search API (http://drupal.org/project/search_api), and,
 * Search API attachments
   (https://www.drupal.org/project/search_api_attachments).

INSTALLATION
------------

1. Download and install the "Search API" module.

2. Download and install the Search API module which provides a service class
   that corresponds with the type of search back-end you plan to use (see
   https://www.drupal.org/node/1254698 for more information).
   
3. Download and install the "Search API Attachments" module. Go to
   admin/config/search/search_api/attachments and configure the method you wish
   to use for extracting the contents of attachments (e.g.: Apache Tika,
   Pdftotext, Pdf2txt, or Apache Solr).

3. Download and install the "Search API Attachments in Body Field" module.

   Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

4. Go to admin/config/search/search_api and add a Search API server and a
   Search API index. Double-check that both the server and index are enabled.

5. Go to admin/config/search/search_api and find the index that you would like
   to start indexing the contents of files referenced in body fields. From that
   index's 'Operations' column, click the drop-down, and select 'Filters'.
   
   You will be taken to a screen that prompts you to select data alterations and
   processors. Under 'Data alterations', select 'File attachments', and click
   'Save configuration'. Then, again under 'Data alterations', select 'File
   attachments Body', and click 'Save configuration'.
   
   Review the 'Data alteration processing order' and '[Data alteration] Callback
   settings' and change if necessary.

6. Click the 'Fields' tab for the index you are editing.

   You will be taken to a screen that prompts you to select fields to index.
   Find the a field with the name 'Attachment content: Body' (machine name
   'attachments_body_file_field'), and ensure that its 'Indexed' checkbox is
   checked, and its 'Type' checkbox is set to 'Fulltext'.

CONFIGURATION
-------------

Similar to the 'File attachments' data alterations, you can configure the
following settings for 'File attachments Body':

 * Excluded file extensions,
 * the mumber of files indexed per file field,
 * the maximum file size to index, and,
 * whether to exclude private files.

You can configure from the 'Data alterations' fieldset on the 'Filters'
configuration page in each Search API index configured on your site.

TROUBLESHOOTING
---------------

We don't know of any problems at this time, so if you find one, please let us
know by adding an issue!

MAINTAINERS
-----------

Current maintainers:
 * Anna Mykhailova (amykhailova) - https://www.drupal.org/user/2892725
 * M Parker (mparker17) - https://www.drupal.org/user/536298

This project has been sponsored by:
 * DIGITAL ECHIDNA
   Since 1999, Digital Echidna (“Echidna”) has worked with many organizations 
   to help them enhance their interactions across a variety of digital channels 
   and improve their overall online marketing presence. The Echidna team is the 
   perfect blend of technical expertise and creative flair which enables us to 
   go beyond the website and create new opportunities for online communication 
   and engagement.
