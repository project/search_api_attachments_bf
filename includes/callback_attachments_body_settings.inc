<?php

/**
 * @file
 * Search API data alteration callback.
 */

/**
 * Settings class for Search Api Attachments in Body settings.
 */
class SearchApiAttachmentsBodyAlterSettings extends SearchApiAttachmentsAlterSettings {

  /**
   * {@inheritdoc}
   */
  public function alterItems(array &$items) {
    // Extension restriction.
    $exclude = array();
    foreach (explode(' ', $this->options['excluded_extensions']) as $ext) {
      $exclude[$ext] = file_get_mimetype('dummy.' . $ext);
    }
    // File size restriction.
    $max_file_size = parse_size($this->options['max_file_size']);

    foreach ($items as &$item) {
      $item_wrapper = entity_metadata_wrapper($this->index->item_type, $item);
      // Case of body field.
      if (isset($item_wrapper->body)) {
        $html = $item_wrapper->body->value();
        $files = array();
        $files = $this->getFilesFromBody($html['value']);;
        // Index the files content.
        if (!empty($files)) {
          // Limit to the max number of value per field.
          if (isset($this->options['number_indexed']) && $this->options['number_indexed'] != '0' && count($files) > $this->options['number_indexed']) {
            $files = array_slice($files, 0, $this->options['number_indexed']);
          }

          foreach ($files as $file) {
            // Private file restriction.
            if (!$this->is_temporary($file) && !($this->options['excluded_private'] && $this->is_private($file))) {
              // Extension restriction.
              if (!in_array($file['filemime'], $exclude)) {
                // File size restriction.
                $file_size_errors = file_validate_size((object) $file, $max_file_size);
                if (empty($file_size_errors)) {
                  $attachments = 'attachments_body_file_field';
                  if (isset($item->{$attachments})) {
                    $item->{$attachments} .= ' ' . $this->getFileContent($file);
                  }
                  else {
                    $item->{$attachments} = $this->getFileContent($file);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function propertyInfo() {
    $ret = array();
    if ($this->index->item_type == 'file') {
      $ret['attachments_content'] = array(
        'label' => 'File content',
        'description' => 'File content',
        'type' => 'text',
      );
    }
    else {
      $ret['attachments_body_file_field'] = array(
        'label' => 'Attachment content: Body',
        'description' => 'Attachment content from the body field',
        'type' => 'text',
      );
    }
    return $ret;
  }

  /**
   * Helper function to get file entities from body field.
   *
   * @param string $text
   *   Html text from body field.
   *
   * @return array
   *   array of files attached in the body field.
   */
  protected function getFilesFromBody($text) {
    $files = array();
    if (isset($text) && !empty($text)) {
      global $base_url;
      // Parse href attributes in <a> links.
      preg_match_all('/href=[\'"]([^\>\'"]*)[\'"]/', $text, $matches, PREG_SET_ORDER);
      foreach ($matches as $match) {
        // Determine if the file is local. Absolute URL could be local.
        // Beginning double slashes is implicit for the current page's protocol
        // but just apply http.
        if (substr($match[1], 0, 2) == '//') {
          $url = 'http:' . $match[1];
        }
        elseif (substr($match[1], 0, 1) == '/') {
          $url = $base_url . $match[1];
        }
        else {
          $url = $match[1];
        }

        $parse = parse_url($url);
        // Get absolute URL to the file location.
        $path_files = file_create_url('public://');
        if (isset($parse['host']) and $parse['host'] == $_SERVER['HTTP_HOST']) {
          $uri = 'public://' . str_replace($path_files, '', $url);
          // Convert back things (such as %20 back to a space).
          $uri = urldecode($uri);

          if (file_exists($uri)) {
            $files_load = file_load_multiple(array(), array('uri' => $uri));
            $file = reset($files_load);
            if ($file) {
              $files[] = (array) $file;
            }
          }
        }
      }
    }
    return $files;
  }

}
